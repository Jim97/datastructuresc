#include <iostream>
using namespace std;

class Node{
     public:
    int key;
    int data;
    Node* next;

   Node(){
      key = 0;
      data = 0;
      next = NULL;
   }

   Node(int k, int d)
   {
      key = k;
      data = d;

      next = NULL;
   }
};

class CircularLinkedList{
   public:
     Node* head;
     CircularLinkedList()
     {
        head = NULL;
     }

     //1. Check if Node Exists Using Key Value
     Node* nodeExists(int k){
        Node* temp = NULL;
        Node* ptr = head;

        if(ptr==NULL){
           return temp;
        }else{
           do{
               if(ptr->key==k){

                   temp=ptr;
               }
               ptr = ptr->next;
           }while(ptr!=head);
           return temp;
        }
     }

     //2. Append node to the list
     void appendNode(Node* new_node){
        if(nodeExists(new_node->key)!=NULL){
           cout<<"Node Exists With Key"<<new_node->key
           <<". Append Node with different key value"
           <<endl;

        }else{
           if(head==NULL)
           {
             head = new_node;
             new_node->next = head;
             cout<<"Node Appended at first head position"<<endl;
           }else{

           }
        }
     }

     void prependNode(Node* new_node){
         if(nodeExists(new_node->key)!=NULL){
           cout<<"Node Exists With Key"<<new_node->key
           <<". Prepend Node with different key value"
           <<endl;
     }else{
          if(head==NULL)
           {
             head = new_node;
             new_node->next = head;
             cout<<"Node prepended at first head position"<<endl;
            }else{
         Node* ptr = head;
         while(ptr->next != head)
         {
            ptr = ptr->next;
         }

         ptr->next = new_node;
         new_node->next = head;
         head = new_node;
         cout<<"Node Prepende"<<endl;

     }
    }
 }
    void insertNodeByKey(int k, Node* new_node)
    {
         Node* ptr = nodeExists(k);
         if(ptr==NULL){
             cout<<"No node exists with key value of: "<<k<<endl;
         }else{
         if(nodeExists(new_node->key)!=NULL){
           cout<<"Node Exists With Key"<<new_node->key
           <<". Insert Node with different key value"
           <<endl;

         }else{
             if(ptr->next=head)
           {
             new_node->next=head;
             ptr->next = new_node;
             cout<<"Node Inserted at the end"<<endl;
         }else{
            new_node->next = ptr->next;
            ptr->next = new_node;
            cout<<"Node Inserted In between"<<endl;
           }
         }

       }
   }
   void deleteNodeByKey(int k)
   {
      Node* ptr = nodeExists(k);
      if(ptr==NULL)
      {
        cout<<"No Node Exists with key value of:"<<k<<endl;
      }
      else{
           if(ptr==head)
        {
           if(head->next==NULL){
              head=NULL;
              cout<<"Head Node Unlinked";
           }else{
              Node* ptr1 = head;
              while(ptr1->next!=head){
                  ptr1 = ptr1->next;
              }
              ptr1->next = head->next;
              head = head->next;
               cout<<" Node Unlinked with Keys Value: "<<k<<endl;
           }
        }else{
            Node* temp = NULL;
            Node* prevptr = head;
            Node* currentptr = head->next;

            while(currentptr!=NULL){
              if(currentptr->key==k){
                  temp = currentptr;
                  currentptr = NULL;
              }else{
                   prevptr = prevptr->next;
                   currentptr = currentptr->next;
              }
            }
            prevptr->next = temp->next;
            cout<<" Node Unlinked with Keys Value: "<<k<<endl;
        }
      }
   }
     void updateNodeByKey(int k, int new_data){
        Node* ptr = nodeExists(k);
        if(ptr!=NULL){
          ptr->data= new_data ;
          cout<<"Node Updated"<<endl;
        }else{
            cout<<"Node Doesn't exist with key value"<<endl;
        }
     }
      void printList(){
         if(head==NULL){
            cout<<"No nodes in the List"<<endl;
         }else{
           cout<<endl<<"Head address of the list"<<head<<endl;
           cout<<"List values: "<<endl;
           Node* temp = head;
           do{
              cout<<"("<<temp->key<<", "<<temp->data<<", "<<temp->next<<") -->";
              temp=temp->next;
           }while(temp!=head);
         }
      }

};

int main(){
    CircularLinkedList obj;
    int option;
   int key1, k1, data1;
   do{
       cout<<"\nWhat Operation do you want to perform? Select option number. Enter 0 to exit"<<endl;
       cout<<"1. appendNode()"<<endl;
       cout<<"2. prependNode()"<<endl;
       cout<<"3. insertNodeByKey()"<<endl;
       cout<<"4. deleteNodeByKey()"<<endl;
       cout<<"5. updateNodeByKey()"<<endl;
       cout<<"6. printList()"<<endl;
       cout<<"7. Clear Screen"<<endl<<endl;

     cin>>option;
     Node* n1 = new Node();
     //Node n1
     switch(option)
        {
          case 0:
             break;
          case 1:
             cout<<"Append Node Operation. \n Enter key and data of the node to be appended"<<endl;
             cin>>key1;
             cin>>data1;
             n1->key=key1;
             n1->data=data1;
             obj.appendNode(n1);
             break;
          case 2:
            cout<<"Prepend Node Operation. \n Enter key and data of the node to be Prepended"<<endl;
            cin>>key1;
             cin>>data1;
             n1->key=key1;
             n1->data=key1;
             obj.prependNode(n1);
             break;
         case 3:
            cout<<"Insert Node after operation \n Enter key of existing Node after which you want to insert new node"<<endl;
            cin>>k1;
            cout<<"Enter key and data of the new Node first"<<endl;
            cin>>key1;
            cin>>data1;
            n1->key=key1;
            n1->data=key1;
            obj.insertNodeByKey(k1,n1);
            break;
        case 4:
            cout<<"Delete Node by Key Operation \n Enter key of the Node to be deleted"<<endl;
            cin>>k1;
            obj.deleteNodeByKey(k1);
            break;
        case 5:
           cout<<"Update Node by Key Operation \n Enter key and new data to be updated"<<endl;
           cin>>key1;
           cin>>data1;
           obj.updateNodeByKey(key1,data1);
           break;
        case 6:
           obj.printList();
          break;
        case 7:
            system("cls");
           break;
        default:
           cout<<"Enter a valid number"<<endl;
        }
   }while(option!=0);
return 0;
}
