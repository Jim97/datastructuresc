#include <iostream>
using namespace std;
class Node{
   public:
    int key;
    int data;
    Node* next;
    Node* previous;

   Node(){
   key=0;
   data = 0;
   next = NULL;
   previous = NULL;

   }
   Node(int k, int d){
   key = k;
   data = d;
   }
};

class DoublyLinkedList{

   public:
     Node* head;

  DoublyLinkedList(){
     head = NULL;
   }
   DoublyLinkedList(Node* n){
     head = n;
   }

  //Check if node exists by key value
   Node* checkIfNodeExists(int k){
       Node* temp = NULL;
       Node* ptr = head;

       while(ptr!=NULL)
       {
          if(ptr->key==k)
          {
             temp = ptr;
          }
          ptr=ptr->next;
       }

       return temp;
   }
   //Append a node
   void appendNode(Node* n){
     if(checkIfNodeExists(n->key!=NULL)){
         cout<<"Node already exist with value"<<endl;
     }else
     if(head==NULL)
     {
       head = n;
       cout<<"Node appended as head node"<<endl;
     }
     else{
        Node* ptr = head;
        while(ptr->next = NULL)
        {
           ptr = ptr->next;

        }
        ptr->next = n;
        n->previous = ptr;
        cout<<"Node appended"<<endl;
     }
   }
   //Prepend node
   void prependNode(Node* n){
      if(checkIfNodeExists(n->key)!=NULL){
        cout<<"Node exists with key value :" <<n->key <<"Append another node with different key value"<<endl;
      }
      else
       {
         head->previous=n;
         n->next=head;
         head = n;
         cout<<"Node prepended"<<endl;
       }
    }

   //Insert a node after a particular node
   void insertNodeAfter(int k,Node* n){
      Node* ptr = checkIfNodeExists(k);
      if(ptr==NULL){
         cout<<"No node exists with key value"<<k<<endl;
       }
       else{
          if(checkIfNodeExists(n->key)!=NULL){
            cout<<"Node exists with key value"<<n->key<<"Append another node with a different key value"<<endl;
          }
          else{
             cout<<"Inserting"<<endl;
             Node* nextNode = ptr->next;
             //Inserting at the end
             if(nextNode==NULL)
             {
               ptr->next=n;
               n->previous = ptr;
               cout<<"Node Inserted at the END"<<endl;
             }
             //Inserting in between
             else{
               n->next = nextNode;
               nextNode->previous=n;
               n->previous = ptr;
               ptr->next = n;
               cout<<"Node Inserted In between"<<endl;
             }

          }
       }
   }

   //Delete node by unique key, basically delink not delete
   void deleteNodeByKey(int k){
      Node* ptr = checkIfNodeExists(k);
      if(ptr==NULL)
      {
        cout<<"No node exists with key value :"<<k<<endl;
      }
        else{
          if(head->key==k)
          {
            head = head->next;
            cout<<"Node Unlinked with Key Value:"<<k<<endl;

          }else{
            Node* nextNode = ptr->next;
            Node* prevNode = ptr->previous;

            //deleting at the end
            if(nextNode==NULL)
            {
              prevNode->next=NULL;
              cout<<"Node deleted at the end"<<endl;
            }
            //Deleting in-between
            else
            {
               prevNode->next=nextNode;
               nextNode->previous = prevNode;
               cout<<"Node deleted in between"<<endl;
            }
          }
        }

   }

       //Update node
       void updateNodeByKey(int k, int d)
       {
           Node* ptr = checkIfNodeExists(k);
           if(ptr!=NULL)
           {
             ptr->data = d;
             cout<<"Node Data Updated Successfully"<<endl;
           }
           else
           {
              cout<<"Node doesn't exist with key value"<<k<<endl;
           }
       }

       //Printing a Node
       void printList()
       {
          if(head==NULL)
          {
             cout<<"No Nodes in doubly Linked List"<<endl;
          }else{
            cout<<endl<<"Doubly Linked List Values: ";
            Node* temp = head;

            while(temp !=NULL)
            {
               cout<<"("<<temp->key<<", "<<temp->data<<")<-->";
               temp = temp->next;
            }
          }
       }
   };



int main(){

   DoublyLinkedList obj;
   int key1, k1, data1;
   int option;
   do{
       cout<<"\nWhat Operation do you want to perform? Select option number. Enter 0 to exit"<<endl;
       cout<<"1. appendNode()"<<endl;
       cout<<"2. prependNode()"<<endl;
       cout<<"3. insertNodeAfter()"<<endl;
       cout<<"4. deleteNodeByKey()"<<endl;
       cout<<"5. updateNodeByKey()"<<endl;
       cout<<"6. printList()"<<endl;
       cout<<"7. Clear Screen"<<endl<<endl;

     cin>>option;
     Node* n1 = new Node();

     //Node n1
     switch(option)
        {
          case 0:
             break;
          case 1:
             cout<<"Append Node Operation. \n Enter key and data of the node to be appended"<<endl;
             cin>>key1;
             cin>>data1;
             n1->key=key1;
             n1->data=key1;
             obj.appendNode(n1);
             break;
          case 2:
            cout<<"Prepend Node Operation. \n Enter key and data of the node to be Prepended"<<endl;
            cin>>key1;
            cin>>data1;
             n1->key=key1;
             n1->data=key1;
             obj.prependNode(n1);
             break;
         case 3:
            cout<<"Insert Node after operation \n Enter key of existing Node after which you want to insert new node"<<endl;
            cin>>k1;
            cout<<"Enter key and data of the new Node first"<<endl;
            cin>>key1;
            cin>>data1;
             n1->key=key1;
             n1->data=key1;
            obj.insertNodeAfter(k1,n1);
            break;
        case 4:
            cout<<"Delete Node by Key Operation \n Enter key of the Node to be deleted"<<endl;
            cin>>k1;
            obj.deleteNodeByKey(k1);
            break;
        case 5:
           cout<<"Update Node by Key Operation \n Enter key and new data to be updated"<<endl;
           cin>>key1;
           cin>>data1;
           obj.updateNodeByKey(key1,data1);
           break;
        case 6:
           obj.printList();
          break;
        case 7:
            system("cls");
           break;
        default:
           cout<<"Enter a valid number"<<endl;
        }
   }while(option!=0);
   return 0;
}


