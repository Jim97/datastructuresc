#include <iostream>
using namespace std;

class Node{

    public:
      int key;
      int data;
      Node* next;

      Node()
      {
         key = 0;
         data = 0;
         next = NULL;
      }

      Node(int k, int d){
          key = k;
          data = d;

      }
};

class SinglyLinkedList
  {
  public:
   Node* head;

   SinglyLinkedList(){
       head = NULL;
   }

   SinglyLinkedList(Node *n){
       head = n;
   }


   //1. Check if node exist
   Node* nodeExists(int k)
   {
      //
      Node* temp = NULL;

      //For head pointer
      Node* ptr = head;

      while(ptr!=NULL){
         if(ptr->key==k){
             temp = ptr;
         }
         ptr = ptr->next;
      }
        return temp;

   }

   //2. Append a node to the list
   void appendNode(Node* n){
       if(nodeExists(n->key)!=NULL){
       cout<<"Node Already exists with key value :"<<n->key<<". Append a node in the rear"<<endl;
       }else
       {
          if(head==NULL)
          {
            head = n;
            cout<<"Node Appended"<<endl;
          }
          else
          {
          Node* ptr = head;
          while(ptr->next!=NULL)
          {
             ptr= ptr->next;
          }
          ptr->next=n;
          }
       }
   }
   //3.Prepend a Node to the list
   void prependNode(Node* n){
      //Check if the node exists
     if(nodeExists(n->key)!=NULL){
       cout<<"Node Already exists with key value :"<<n->key<<". Append a node in the rear"<<endl;
       } else{
        //Point the head pointer to the existing first node after adding a node
        n->next=head;
        //Point the head to the added object
        head = n;
        cout<<"Node Prepended"<<endl;
       }
   }
   //4.Insert a Node after a particular Node
   void insertNode(int k, Node* n){

      //Check by key if the particular node exists and store it in pointer
        Node* ptr = nodeExists(k);
        //Ensure that ptr
        if(ptr == NULL){
          cout<<"A node exists with the particular"<<k<<endl;
        }else{
         //Check if the node exists
        if(nodeExists(n->key)!=NULL){
       cout<<"Node Already exists with key value :"<<n->key<<". Append a node in the rear"<<endl;
       }else{

         n->next = ptr->next;
         ptr->next = n;
          cout<<"Node Inserted"<<endl;
       }

        }
   }

   //5.Delete Node by a unique key

   void deleteByKey(int k){
       if(head==NULL){
           cout<<"Singly Linked List Already Empty"<<endl;
       }
       else if(head!=NULL)
       {
          if(head->key==k)
          {
              head = head->next;
              cout<<"Node Unlinked With Keys and Value"<<k<<endl;
          }else{
            Node* temp = NULL;
            Node* prevptr = head;
            Node* currentptr = head->next;
            while(currentptr!=NULL){
                if(currentptr->key==k)
                {
                    temp = currentptr;
                    currentptr = NULL;
                }else{
                   prevptr = prevptr->next;
                   currentptr = currentptr->next;

                }
            }
            if(temp!=NULL)
            {
              prevptr->next = temp->next;
              cout<<"Node Unlinked With Keys Value:"<<k<<endl;
            }else{
               cout<<"Node Exist With Keys Value:"<<k<<endl;
            }

          }
       }
   }
   //Update Node by Key
   void updateNode(int k, int d){
      Node* ptr = nodeExists(k);
      if(ptr!=NULL)
      {
         ptr->data= d;
         cout<<"Node Data Updated Successfull"<<endl;
      }else{
        cout<<"Node Data Doesn't Exist :"<<k<<endl;
      }
   }
   //Printing List
   void printList()
   {
     if(head==NULL)
     {
       cout<<"No nodes in the list"<<endl;
     }
       else
       {
          cout<<endl<<"Singly Linked List Values: ";
          Node* temp = head;

          while(temp != NULL)
           {
              cout<<"("<<temp->key<<", "<<temp->data<<") --> ";
              temp = temp->next;
           }
       }
   }
};

int main(){
    SinglyLinkedList s;
    int option;
    int key1,k1,data1;

    do{

    cout<<"\nSelect What Operation Do You Want To Perform? Select option number. Enter 0 to exit."<<endl;
    cout<<"1. appendNode()"<<endl;
    cout<<"2. prependNode()"<<endl;
    cout<<"3. insertNode()"<<endl;
    cout<<"4. deleteByKey()"<<endl;
    cout<<"5. updateNode()"<<endl;
    cout<<"6. printList()"<<endl;
    cout<<"7. Clear Screen"<<endl<<endl;

    cin>>option;
    Node* n1 = new Node();

    switch(option){
    case 0:
       break;
    case 1:
        cout<<"Append node operation. \nEnter key and data to be appended"<<endl;
        cin>>key1;
        cin>>data1;
        n1->key=key1;
        n1->data= data1;
        s.appendNode(n1);
        //cout<<n.key<<"="<<n1.data<<endl;
        break;
     case 2:
     cout<<"Prepend node operation. \nEnter key and data to be Prepended"<<endl;
        cin>>key1;
        cin>>data1;
        n1->key=key1;
        n1->data= data1;
        s.prependNode(n1);
        //cout<<n.key<<"="<<n1.data<<endl;
        break;
     case 3:
       cout<<"Insert node after operation. \nEnter key of existing node after which new node to be inserted: "<<endl;
       cin>>k1;
       cout<<"Enter key and data of the new node"<<endl;
       cin>>key1;
       cin>>data1;
       n1->key=key1;
       n1->data= data1;
       s.insertNode(k1,n1);
       break;
     case 4:
        cout<<"Delete Node by key operation - \nEnter key of the node to be deleted"<<endl;
        cin>>k1;
        s.deleteByKey(k1);
        break;
    case 5:
       cout<<"Update Node by key operation - \nEnter key and  new data to be updated"<<endl;
       cin>>key1;
       cin>>data1;
       s.updateNode(key1,data1);
       break;
    case 6:
       s.printList();
       break;
    case 7:
      system("cls");
      break;
    default:
    cout<<"Enter Valid Number"<<endl;
    }

    }while(option!=0);
   return 0;
}
