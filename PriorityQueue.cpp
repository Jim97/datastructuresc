#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;

struct node{
 int priority;
 int info;
  struct node *link;
};

class Priority_Queue{
  private:
    node *frnt;

  public:
  Priority_Queue(){
     frnt = NULL;
  }

  void inst(int item, int priority){
    node *tmp, *q;
    tmp = new node;
    tmp->info = item;
    tmp->priority = priority;
    if(frnt == NULL || priority <frnt-> priority)
    {
      tmp->link = frnt;
      frnt = tmp;
    }
    else
    {
      q = frnt;
      while(q->link != NULL && q->link->priority <= priority)
        q=q->link;
      tmp->link = q->link;
      q->link = tmp;
    }
  }
  void del(){
   node *tmp;
   if(frnt==NULL)
   cout<<"Queue Underflow\n"<<endl;
   else
   {
     tmp = frnt;
     cout<<"Deleted item is: "<<tmp->info<<endl;
     frnt = frnt->link;
     free(tmp);
   }
  }

  /*
  * Print priority queue
  */

  void display(){
    node *ptr;
    ptr = frnt;
    if(frnt ==NULL)
      cout<<"Queue is empty\n"<<endl;
  else{
    cout<<"Queue is :\n"<<endl;
    cout<<"Priority Item\n";
    while(ptr->link !=NULL)
    {
     ptr = ptr->link;
    }
    cout<<ptr->priority<<"   "<<ptr->info<<endl;

  }

 }
};
int main(){
  int option, item, priority;
  Priority_Queue pq;

  do{
    cin>>option;
   cout<<"\n What operation do you want to perform \n"<<endl;
   cout<<"1. inst()\n"<<endl;
   cout<<"2. del()\n"<<endl;
   cout<<"3. display()\n"<<endl;
   cout<<"4. Clear Screen\n"<<endl<<endl;

  switch(option){
      case 0:
         break;
    case 1:
       cout<<"Enter the item to be Inserted"<<endl;
       cin>>item;
       cout<<"Enter its priority"<<endl;
       cin>>priority;
       pq.inst(item, priority);
       break;
    case 2:
      pq.del();
      break;
    case 3:
      pq.display();
      break;
    case 4:
       system("clear");
       break;
    default:
      cout<<"\n Enter valid number"<<endl;
  }


  }while(option != 0);
}


