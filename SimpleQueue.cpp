#include<iostream>;

using namespace std;

class SimpleQueue{
    private:
       int Front;
       int rear;
       int arr[5];

       public:

       SimpleQueue(){
       Front = -1;
       rear = -1;

       for(int i = 0; i<5; i++){
            cout<<"Enter Number Into The Queue"<<endl;
          cin>>arr[i];
       }

       }

       bool isEmpty(){
         if(Front==-1&&rear == -1)
              return true;
         else
              return false;
       }

       bool isFull(){
          if(rear==4)
             return true;
          else
             return false;

       }

       void enqueue(int value){
           if(isFull()){
              return;
           }else if(isEmpty()){
               Front = 0;
               rear = 0;
               arr[rear] = value;
           }else{

           rear++;
           arr[rear]= value;

           }


       }

       int dequeue(){
             int x;
         if(isEmpty()) {
            cout<<"Queue Is Empty"<<endl;
            return 0;
         }else if(Front=rear){
               x = arr[Front];
              arr[Front] = 0;
              rear = -1;
              Front = -1;

              return x;
         }else{
            x = arr[Front];
            arr[Front] = 0;
            Front++;
            return x;
         }
       }

      int Count(){
           return(rear-Front +1 );
      }
      void display(){
         cout<<"All Items In The array are:"<<endl;
         for(int i=0;i<5;i++){
           cout<<arr[i]<<" ";
         }
      }

};

int main()
{
  SimpleQueue q1;
  int option;
  int val;

   do
   {
     cout<<"\n\nWhat Operation Do You Want To Perform? Select Option Number. Enter 0 To Exit."<<endl;
     cout<<"1. Enqueue()"<<endl;
     cout<<"2. Dequeue()"<<endl;
     cout<<"3. isEmpty()"<<endl;
     cout<<"4. isFull()"<<endl;
     cout<<"5. count()"<<endl;
     cout<<"6. display()"<<endl;
     cout<<"7. Clear Screen"<<endl<<endl;

     cin>>option;

     switch(option)
     {
     case 0:

        break;
     case 1:
          cout<<"Enter Value You Want To Add Into The Queue"<<endl;
          cin>>val;
          q1.enqueue(val);
        break;
     case 2:
         cout<<"Remove A Value From The Queue: "<< q1.dequeue()<<endl;
        break;
     case 3:
         if(q1.isEmpty()){
            cout<<"Queue Is Empty"<<endl;
         }else{
            cout<<"Queue Is not Empty"<<endl;
         }

       break;
     case 4:
           if(q1.isFull()){
            cout<<"Queue Is Full"<<endl;
         }else{
            cout<<"Queue Is not Full"<<endl;
         }
       break;
     case 5:
         cout<<"The Number Of Items In The Queue Is"<<q1.Count()<<endl;
       break;
     case 6:
         cout<<"Display Items In The Array"<<endl;
         q1.display();
       break;
      case 7:
          system("cls");
       break;
      default:
        cout<<"Enter Valid Option"<<endl;
        break;
     }
   }while(option!=0);
   return 0;

}
